//
//  DetailViewController.swift
//  PunkCervejas
//
//  Created by Gustavo Melki on 27/08/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    var beer : Beer?
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var abv: UILabel!
    @IBOutlet weak var ibu: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let name = beer?.name, let imgUrl = beer?.image_url, let tagline = beer?.tagline,
            let abv = beer?.abv, let ibu = beer?.ibu, let desc = beer?.description {
            
            self.imgView.kf.setImage(with: URL(string: (imgUrl)))
            self.name.text = name
            self.tagline.text = tagline
            self.abv.text = String(abv)
            self.ibu.text = String(ibu)
            self.desc.text = desc
        }
    }
}
