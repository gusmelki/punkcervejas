//
//  Beer.swift
//  PunkCervejas
//
//  Created by Gustavo Melki on 27/08/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation

public struct Beer: Codable {
    
    public let id: Int?
    public let name: String?
    public let image_url: String?
    public let tagline: String?
    public let abv: Double?
    public let ibu: Double?
    public let description: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case image_url
        case tagline
        case abv
        case ibu
        case description
        
    }
    
    
}
