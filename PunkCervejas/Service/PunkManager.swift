//
//  PunkManager.swift
//  PunkCervejas
//
//  Created by Gustavo Melki on 27/08/19.
//  Copyright © 2019 Gustavo Melki. All rights reserved.
//

import Foundation
import Moya
import RxSwift

public class PunkManager {
    
    static let provider = MoyaProvider<PunkService>()
    
    public static func getBeers() -> Observable<[Beer]>{
        return self.provider.rx.request(.beers)
            .asObservable()
            .filterSuccessfulStatusCodes()
            .map([Beer].self)
    }
}
